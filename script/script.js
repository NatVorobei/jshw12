let btn = document.querySelectorAll('.btn');
let btnE = document.getElementById('e');
let btnO = document.getElementById('o');
let btnN = document.getElementById('n');
let btnS = document.getElementById('s');
let btnL = document.getElementById('l');
let btnZ = document.getElementById('z');
let btnEnter = document.getElementById('enter');


document.addEventListener("keydown", function(e) {
    if(e.key === 'Enter'){
        btnEnter.classList.add('active');
    } else {
        btnEnter.classList.remove('active');
    } 

    if (e.key === 'S' || e.key === 's') {
        btnS.classList.add('active');
    } else{
        btnS.classList.remove('active');
    }

    if (e.key === 'E' || e.key === 'e') {
        btnE.classList.add('active');
    } else{
        btnE.classList.remove('active');
    }

    if (e.key === 'L' || e.key === 'l') {
        btnL.classList.add('active');
    } else{
        btnL.classList.remove('active');
    }

    if (e.key === 'Z' || e.key === 'z') {
        btnZ.classList.add('active');
    } else{
        btnZ.classList.remove('active');
    }

    if (e.key === 'N' || e.key === 'n') {
        btnN.classList.add('active');
    } else{
        btnN.classList.remove('active');
    }

    if (e.key === 'O' || e.key === 'o') {
        btnO.classList.add('active');
    } else{
        btnO.classList.remove('active');
    }


});


